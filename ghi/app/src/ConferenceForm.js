import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [name, setName] = useState('');
  const [starts, setStartDate] = useState('');
  const [ends, setEndDate] = useState('');
  const [description, setDescription] = useState('');
  const [max_presentations, setMaxPresentations] = useState('');
  const [max_attendees, setMaxAttendees] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]); // State to store locations data

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleStartDateChange = (event) => {
    const value = event.target.value;
    setStartDate(value);
  }

  const handleEndDateChange = (event) => {
    const value = event.target.value;
    setEndDate(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaxPresentationChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }

  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name,
      starts,  // Should be in the format "YYYY-MM-DD"
      ends,    // Should be in the format "YYYY-MM-DD"
      description,
      max_presentations,
      max_attendees,
      location: parseInt(location),
    };

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(conferenceUrl, fetchConfig);

      if (response.ok) {
        const newConference = await response.json();

        // Clear form inputs and reset state
        setName('');
        setStartDate('');
        setEndDate('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');
        setLocation('');

      } else {
        console.error('Conference API Error:', response.status, response.statusText);
      }
    } catch (error) {
      console.error('Fetch error:', error);
    }
  }


  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form id="create-conference-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartDateChange} placeholder="Start date" required type="date" name="starts" id="starts" className="form-control"/>
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndDateChange} placeholder="End date" required type="date" name="ends" id="ends" className="form-control"/>
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="form-floating mb-3">
            <textarea onChange={handleDescriptionChange} placeholder="Description" required name="description" id="description" className="form-control"></textarea>
            <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxPresentationChange} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
              <label htmlFor="max_presentations">Maximum Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
              <label htmlFor="max_attendees">Maximum Attendees</label>
            </div>
            <div className="mb-3">
              <select required name="location" id="location" className="form-select" onChange={handleLocationChange}>
                <option value="">Choose a location</option>
                {locations.map(loc => (
                  <option key={loc.id} value={loc.id}>
                    {loc.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
