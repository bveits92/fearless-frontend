import React, { useEffect, useState } from 'react';

function PresentationForm() {
  const [presenterName, setPresenterName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [selectedConference, setSelectedConference] = useState('');
  const [conferences, setConferences] = useState([]);

  useEffect(() => {
    async function fetchConferences() {
      try {
        const response = await fetch('http://localhost:8000/api/conferences/');
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        }
      } catch (error) {
        console.error('Error fetching conferences:', error);
      }
    }

    fetchConferences();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Create a presentation object
    const presentationData = {
      presenter_name: presenterName,
      presenter_email: presenterEmail,
      company_name: companyName,
      title,
      synopsis,
      conferenceId: selectedConference,
    };

    try {
      const response = await fetch(`http://localhost:8000/api/conferences/${selectedConference}/presentations/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(presentationData),
      });

      if (response.ok) {
        // Presentation was successfully created
        setPresenterName('');
        setPresenterEmail('');
        setCompanyName('');
        setTitle('');
        setSynopsis('');
        setSelectedConference('');
      } else {
        // Handle error if needed
        console.error('Failed to create presentation.');
      }
    } catch (error) {
      console.error('Error creating presentation:', error);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Presentation</h1>
          <form id="create-presentation-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Presenter Name"
                value={presenterName}
                onChange={(e) => setPresenterName(e.target.value)}
                required
              />
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Presenter Email"
                value={presenterEmail}
                onChange={(e) => setPresenterEmail(e.target.value)}
                required
              />
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Presenter Company"
                value={companyName}
                onChange={(e) => setCompanyName(e.target.value)}
                required
              />
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                required
              />
              <label htmlFor="title">Title</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                className="form-control"
                placeholder="Synopsis"
                value={synopsis}
                onChange={(e) => setSynopsis(e.target.value)}
                required
              />
              <label htmlFor="description">Synopsis</label>
            </div>
            <div className="mb-3">
              <select
                required
                name="conference"
                id="conference"
                className="form-select"
                value={selectedConference}
                onChange={(e) => setSelectedConference(e.target.value)}
              >
                <option value="">Choose a conference</option>
                {conferences.map((conference) => (
                  <option key={conference.id} value={conference.id}>
                    {conference.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
