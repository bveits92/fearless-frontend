import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/mainpage/new">Conference GO!</NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
            <NavLink className="nav-link" exact="true" to="/mainpage/new">Home</NavLink>

            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/locations/new">New Location</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/conferences/new">New conference</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/presentations/new">New presentation</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/attendees">Attendee list</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
