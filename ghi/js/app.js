function formatDate(date) {
  const options = { year: 'numeric', month: 'short', day: 'numeric' };
  return new Date(date).toLocaleDateString(undefined, options);
}

function createCard(name, description, pictureUrl, startDate, endDate, location) {
  const formattedStartDate = formatDate(startDate);
  const formattedEndDate = formatDate(endDate);

  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-title">
          <small class="text-muted">${location}</small>
        </p>
        <p class="card-text">${description}</p>
        <p class="card-text">${formattedStartDate} - ${formattedEndDate}</p>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('Bad response'); // Throw an error with a specific message
    }

    const data = await response.json();
    const cardContainer = document.querySelector('.row.row-cols-3');

    for (let conference of data.conferences) {
      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);

      if (detailResponse.ok) {
        const details = await detailResponse.json();
        const title = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const startDate = details.conference.starts;
        const endDate = details.conference.ends;
        const location = details.conference.location.name;
        const html = createCard(
          title,
          description,
          pictureUrl,
          startDate,
          endDate,
          location,
        );

        // Create a new column for each card
        const col = document.createElement('div');
        col.classList.add('col'); // Each card gets its own column
        col.innerHTML = html;

        // Append the column (and the card) to the cardContainer
        cardContainer.appendChild(col);
      }
    }
  } catch (e) {
    // Handle the error when an exception is raised
    const errorContainer = document.querySelector('.error-container');
    errorContainer.innerHTML = `<div class="alert alert-danger" role="alert">Error: ${e.message}</div>`;
  }
});
