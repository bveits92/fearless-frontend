window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';

    try {
      const locationResponse = await fetch(locationUrl);

      if (locationResponse.ok) {
        const locationData = await locationResponse.json();

        const selectTag = document.getElementById('location');
        for (let location of locationData.locations) {
          // Create an 'option' element
          const option = document.createElement('option');

          // Set the '.value' property of the option element to the location's id
          option.value = location.id;

          // Set the '.textContent' property of the option element to the location's name
          option.textContent = location.name;

          // Append the option element as a child of the select tag
          selectTag.appendChild(option);
        }
      } else {
        console.error('Location API Error:', locationResponse.status, locationResponse.statusText);
      }

      const formTag = document.getElementById('create-conference-form');
      formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: 'POST',
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        try {
          const conferenceResponse = await fetch(conferenceUrl, fetchConfig);

          if (conferenceResponse.ok) {
            formTag.reset();
            const newConference = await conferenceResponse.json();
            console.log(newConference);
          } else {
            console.error('Conference API Error:', conferenceResponse.status, conferenceResponse.statusText);
          }
        } catch (error) {
          console.error('Fetch error:', error);
        }
      });
    } catch (error) {
      console.error('Fetch error:', error);
    }
  });
